<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 08/12/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\firefly\services\fileManager;

use yii\web\NotFoundHttpException;

class FileNotFoundException extends NotFoundHttpException
{
}