<?php

namespace neon\firefly\services\mediaManager;

use \yii\web\ConflictHttpException;

class DirectoryExistsException extends ConflictHttpException
{
}