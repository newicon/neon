module.exports = {
	outputDir: './publish/dist',
	lintOnSave: false,
	runtimeCompiler: true,
	css: {
		extract: true
	}
}
