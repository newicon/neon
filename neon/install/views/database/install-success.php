<?php use yii\helpers\Url as Url; ?>
<div class="media">
	<div class="media-left">
		<div class="media-object">
			<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:green;font-size:35px;"></span>
		</div>
	</div>
	<div class="media-body">
		<!-- @test: database successfully installed -->
		<strong>The database has been successfully installed!</strong> We now need to set up an administrative account to access and administer the application.
	</div>
</div>


