{markdown}
# Supported Documentation Files

You can write documentation using smarty (.tpl), twig (.twig), php (.php), markdown (.md) or any supported view file
format that has a registered renderer matching the extension.  - see the View configuration component and associates
renderers.

{/markdown}

