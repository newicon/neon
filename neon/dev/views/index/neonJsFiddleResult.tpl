<html>
	<head>
		{neon_head}
		{registerAsset bundle="\neon\core\assets\CoreAsset"}
		<style>
			{$style}
		</style>
	</head>
	<body>
		{$html}
		{$scripts}
		<script>
			{$script}
		</script>
	</body>
</html>
