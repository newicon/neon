// vue.config.js
module.exports = {
	// yii will add ?v=timestamp to ensure modified js files are loaded over cached ones
	filenameHashing: false
}
