<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2018- Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\phoebe\services\adapters\basic;

use neon\phoebe\services\adapters\common\PhoebeObjectBase;

/**
 * Provides the basic phoebe object with no additional functionality
 */
class PhoebeObject extends PhoebeObjectBase
{
}