<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 15/09/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\core\helpers;

use \neon\core\helpers\Arr;

/**
 * @deprecated use Arr instead
 * Class ArrayHelper
 * @package neon\core\helpers
 */
class ArrayHelper extends Arr
{

}