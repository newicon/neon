<?php

namespace neon\core\controllers;

use neon\core\helpers\Hash;
use neon\core\grid\GridBase;
use neon\core\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class GridController extends Controller
{
	/**
	 * @param $token
	 * @throws BadRequestHttpException
	 * @throws \yii\web\NotFoundHttpException
	 * @throws \yii\web\RangeNotSatisfiableHttpException
	 */
	public function actionExportCsv($token)
	{
		$grid = Hash::getObjectFromToken($token, GridBase::class);
		$this->exportCsvFile($grid);
	}

	public function exportCsvFile($grid)
	{
		$page = 0;
		$csvFile = $this->createCSVFile();
		do {
			set_time_limit(30);
			$rows = $grid->getCsvData($page++);
			$hasRows = !empty($rows);
			file_put_contents($csvFile, $rows."\n", FILE_APPEND);
			unset($rows);
		} while ($hasRows === true);
		$handle = fopen($csvFile, 'r');
		if ($handle)
			neon()->response->sendStreamAsFile($handle, $grid->title.'.csv')->send();
		@unlink($csvFile);
	}

	private function createCSVFile()
	{
		// create a temp directory
		$tmpDir = \Yii::$app->runtimePath . '/csvtmp';
		if (!is_dir($tmpDir) && (!@mkdir($tmpDir) && !is_dir($tmpDir))) {
			throw new \yii\web\NotFoundHttpException('temp directory does not exist');
		}
		return "$tmpDir/".Hash::uuid64().".csv";
	}

	/**
	 * Process grid filter actions
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 * @throws \yii\web\NotFoundHttpException
	 */
	public function actionFilter($token)
	{
		$grid = Hash::getObjectFromToken($token, GridBase::class);
		return $grid->run();
	}
}
