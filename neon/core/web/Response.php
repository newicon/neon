<?php


/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 27/09/2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 */

namespace neon\core\web;

use yii\web\RangeNotSatisfiableHttpException;

/**
 * Class Response
 * @author Steve O'Brien - Newicon Ltd
 * @package neon\core\web
 */
class Response extends \yii\web\Response
{
	public $formatters = [
		self::FORMAT_JSON => '\neon\core\web\JsonResponseFormatter'
	];

	/**
	 * Prepares for sending the response.
	 * The default implementation will convert [[data]] into [[content]] and set headers accordingly.
	 * @throws \InvalidArgumentException if the formatter for the specified format is invalid or [[format]] is not supported
	 */
	protected function prepare()
	{
		// check if we should guess the format
		// mainly if we should convert the response to be json
		if ($this->shouldBeJson($this->data) && $this->format == self::FORMAT_HTML) {
			$this->format = self::FORMAT_JSON;
		}

		parent::prepare();
	}

	/**
	 * Set the response format to be JSON
	 */
	public function asJson()
	{
		$this->format = self::FORMAT_JSON;
	}

	/**
	 * Determine if the given content should be turned into JSON.
	 *
	 * @param  mixed  $content
	 * @return bool
	 */
	protected function shouldBeJson($content)
	{
		return !is_string($content) && $content !== null;
	}

	public function sendStreamAsFile($handle, $attachmentName, $options = [])
	{
		$headers = $this->getHeaders();
		if (isset($options['fileSize'])) {
			$fileSize = $options['fileSize'];
		} else {
			if ($this->isSeekable($handle)) {
				fseek($handle, 0, SEEK_END);
				$fileSize = ftell($handle);
				fseek($handle, 0, SEEK_SET); // reset to beginning of the file
			} else {
				$fileSize = 0;
			}
		}

		$range = $this->getHttpRange($fileSize);
		if ($range === false) {
			$headers->set('Content-Range', "bytes */$fileSize");
			throw new RangeNotSatisfiableHttpException();
		}

		list($begin, $end) = $range;
		// only allow 3MB to be streamed at a time
		$maxSize = 3 * 1024 * 1024; // 3MB
		if (($end - $begin + 1) > $maxSize) {
			$end = $begin + $maxSize - 1;
		}
		$length = $end - $begin + 1;

		if ($begin != 0 || $end != $fileSize - 1) {
			$this->setStatusCode(206);
			$headers->set('Content-Range', "bytes $begin-$end/$fileSize");
		} else {
			$this->setStatusCode(200);
		}

		$mimeType = isset($options['mimeType']) ? $options['mimeType'] : 'application/octet-stream';
		$this->setDownloadHeaders($attachmentName, $mimeType, !empty($options['inline']), $length);

		$this->format = self::FORMAT_RAW;
		$this->stream = [$handle, $begin, $end];

		$headers->set('Content-Length', $length);
		$headers->set('Accept-Ranges', 'bytes');

		if (ob_get_length()) ob_end_clean();
		$this->sendHeaders();

		fseek($handle, $begin);
		$bufferSize = 524288; // 0.5 MB buffer size
		while (!feof($handle) && ($pos = ftell($handle)) <= $end) {
			if ($pos + $bufferSize > $end) {
				$bufferSize = $end - $pos + 1;
			}
			echo fread($handle, $bufferSize);
			flush();
		}

		fclose($handle);

		return $this;
	}
}
