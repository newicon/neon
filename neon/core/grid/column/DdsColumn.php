<?php
/**
 * @link http://www.newicon.net/
 * @copyright Copyright (c) 2015 Newicon Ltd
 * @license http://www.newicon.net/neon/framework/license/
 * @author Steve O'Brien <steve.obrien@newicon.net>
 * @date 12/12/2015 16:13
 */
namespace neon\core\grid\column;

use neon\core\form\interfaces\IField;
use neon\core\grid\query\IQuery;
use neon\core\helpers\Arr;

/**
 * Class DdsColumn
 * @package neon\core\grid\column
 * @deprecated - all functionality is provided by the Column base class plus a suitable form field member
 */
class DdsColumn extends Column
{
}