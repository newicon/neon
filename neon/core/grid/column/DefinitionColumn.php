<?php
/**
 * Created by PhpStorm.
 * User: newicon
 * Date: 11/04/2018
 * Time: 19:34
 */

namespace neon\core\grid\column;

use \neon\core\form\interfaces\IField;

/**
 * Class DefinitionColumn
 * @package neon\core\grid\column
 * @deprecated - all functionality is provided by the Column base class plus a suitable form field member
 */
class DefinitionColumn extends Column
{
}