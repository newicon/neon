<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 06/12/2016 19:27
 * @package neon
 */
?>

<h1>403! <?= $message ?></h1>


<p>You do not have permission to do whatever it was you were trying to do. Sorry.</p>
<p>You can return to the <a href="<?= neon()->getHomeUrl() ?>">home page by clicking here</a></p>