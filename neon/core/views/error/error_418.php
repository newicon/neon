<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 06/12/2016 19:27
 * @package neon
 */
?>

<h1>418 - I'm a Teapot!</h1>
<p>No coffee allowed here! <?= $message ?></p>
