<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 06/12/2016 19:27
 * @package neon
 */
?>

<h1>Bad Request (#400)</h1>

<p>You can return to the <a href="<?= neon()->getHomeUrl() ?>">home page by clicking here</a></p>