<?php
/**
 * @link http://www.newicon.net/neon
 * @copyright Copyright (c) 2016 Newicon Ltd
 * @license http://www.newicon.net/neon/license/
 * @author Steve O'Brien <steve.obrien@newicon.net> 06/12/2016 18:12
 * @package neon
 */
?>
<h1>Oops an error occurred <?= $status; ?></h1>
<p>A server error occurred. The error was logged.</p>
