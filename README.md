[![Latest Stable Version](https://poser.pugx.org/newicon/neon/v)](//packagist.org/packages/newicon/neon) [![Build Status](https://scrutinizer-ci.com/b/newicon/neon/badges/build.png?b=releases)](https://scrutinizer-ci.com/b/newicon/neon/build-status/releases) [![Code Coverage](https://scrutinizer-ci.com/b/newicon/neon/badges/coverage.png?b=releases)](https://scrutinizer-ci.com/b/newicon/neon/?branch=releases) [![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/newicon/neon/badges/quality-score.png?b=releases)](https://scrutinizer-ci.com/b/newicon/neon/?branch=releases) [![License](https://poser.pugx.org/newicon/neon/license)](//packagist.org/packages/newicon/neon)

Neon
====

Neon is a software development framework developed and maintained by [Newicon.net](https://newicon.net).  
For more info check out the website [https://newicon.net/neon](https://newicon.net/neon).



This is the core Neon Framework.

This is repository should not be included directly in your application.

Use the `neon-app` repository to start a new application project using this.
